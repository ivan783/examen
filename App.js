import React from 'react'
import { View, Text, } from 'react-native'
import tw from 'tailwind-react-native-classnames';
import CardComponents from './src/components/CardComponents';
import UserList from './src/components/UserList';

const App = () => {

  return (
    <View  >
      <Text style={tw`text-center text-xl text-gray-800`}>Examen de Apay</Text>
    {/*<CardComponents/>*/}
    <UserList />
    </View>
  )
}

export default App
