import React from 'react'
import firebase from "../database/firebase"

import { View, Text } from 'react-native'
import { useEffect, useState } from "react"

const UserList = () => {
    
  const [todoList, setTodoList] = useState();
  useEffect (() => {
      const todoRef = firebase.database().ref('Todo');
      todoRef.on('value', (snapshot) => {
          console.log(snapshot.val());
          const todos = snapshot.val();
          const todoList = [];
          for (let id in todos) {
              todoList.push(todos[id])
          }
          console.log(todoList);
          setTodoList(todoList)

      });
  }, [])
    return (
        <View>{todoList ? todoList.map((todo) => <Text>{todo.name}</Text>) : ''}</View>
    )
}

export default UserList
