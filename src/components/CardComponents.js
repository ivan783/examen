import React, { useState } from 'react'
import { View, TextInput, Button } from 'react-native'
import tw from 'tailwind-react-native-classnames';
import firebase from "../database/firebase"
const CardComponents = () => {

    const [state, setState] = useState({
        name: '',
        lastName: '',
        date: '',
    });

    const handleChangeText = (name, value) => {
        setState({ ...state, [name]: value })
    };
    //conectando a la base de datos

    const saveNewUser = async () => {
        if (state.name === '') {
            alert('por favor ingrese los datos')
        }else{
           const todoRef = firebase.database().ref('Todo');
           const todo = {
            name: state.name,
            lastName: state.lastName,
            date: state.date
           }
           todoRef.push(todo)
            alert('se guardo exitosamente');
        }
    } 
    return (
        <View>
              <View style={tw`pt-2 bg-blue-300 rounded-3xl mb-4 mt-4`}>
            <TextInput style={tw`text-center text-xl`} placeholder="Nombre" 
            onChangeText={(value) => handleChangeText('name', value)}/>
        </View>
        <View style={tw`pt-2 bg-green-300 rounded-3xl mb-4`}>
            <TextInput style={tw`text-center text-xl`} placeholder="Apellido"
            onChangeText={(value) => handleChangeText('lastName', value)}/>
        </View>
        <View style={tw`pt-2 bg-yellow-300 rounded-3xl mb-6`}>
            <TextInput style={tw`text-center text-xl`} placeholder="Edad"
            onChangeText={(value) => handleChangeText('date', value)}/>
        </View>

        <Button title="Gardar" onPress={() => saveNewUser()} />
       </View>
    )
}

export default CardComponents
