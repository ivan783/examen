import React from 'react'

function Todo({ todo }) {
    const deleteTodo = () => {
        const todoRef = firebase.database().ref('Todo').child(todo.id);
        todoRef.remove();
    };
    const completeTodo = () => {
        const todoRef = firebase.database().ref('Todo').child(todo.id);
        todoRef.update({
            complete: !todo.complete,
        });
    }
    return (
        <View>
            <Text>{todo.name}</Text>
            <Button title="Editar" onPress={() => deleteTodo()} />
            <Button title="Eliminar" onPress={() =>completeTodo()}/>
        </View>
    )
}

export default Todo
