import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyBQhHDS_Kl-za7eAH_Y2t5QXqtVIpN3wBw",
    authDomain: "examen-apay.firebaseapp.com",
    projectId: "examen-apay",
    storageBucket: "examen-apay.appspot.com",
    messagingSenderId: "194659142691",
    appId: "1:194659142691:web:62e0f0ec72754d29c9b596",
    measurementId: "G-P9FKRE2BM1"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;